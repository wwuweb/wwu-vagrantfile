#!/bin/bash

modules_dir='/vagrant/puppet/modules'

mkdir -p $modules_dir

(puppet module list | grep npacker-composer)  || puppet module install --target-dir $modules_dir npacker-composer
(puppet module list | grep puppetlabs-apt)    || puppet module install --target-dir $modules_dir puppetlabs-apt    --version '2.4.0'
(puppet module list | grep puppetlabs-concat) || puppet module install --target-dir $modules_dir puppetlabs-concat --version '1.2.5'
(puppet module list | grep puppetlabs-nodejs) || puppet module install --target-dir $modules_dir puppetlabs-nodejs --version '0.8.0'
(puppet module list | grep puppetlabs-stdlib) || puppet module install --target-dir $modules_dir puppetlabs-stdlib
